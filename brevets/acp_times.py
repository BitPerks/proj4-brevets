"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import logging


#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#
brevet_end_list = {
    '0':{'max_time': 13.5, 'min_open': 34,'max_close': 15, 'list_val': 0},
    '200':{'max_time': 13.5, 'min_open': 32, 'max_close': 15, 'list_val': 1},
    '300':{'max_time': 20.0, 'min_open': 32, 'max_close': 15, 'list_val': 2},
    '400':{'max_time': 27.0, 'min_open': 30, 'max_close': 15, 'list_val': 3},
    '600':{'max_time': 40.0, 'min_open': 28, 'max_close': 11.428, 'list_val': 4},
    '1000':{'max_time': 75.0, 'min_open': 26, 'max_close': 13.333, 'list_val': 5}
}
#brevet_minimum_list = {'0':15, '200':15,'300':15,'400':15,'600':11.428,'1000':13.333}
#brevet_maximum_list ={'0':34,'200':32,'300':32,'400':30,'600':28,'1000':26}
brevet_list = [0,200,300,400,600,1000]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if str(brevet_dist_km) not in brevet_end_list:
        return "Proper Time"
    if control_dist_km <= 0:
        return "Positive Integer"
    try:
        dist_calculated = 0.0
        dist_whole = 0
        dist_partial = 0.0
        brevet_adj = brevet_end_list[str(brevet_dist_km)]
        brevet_start_time = arrow.get(str.format(brevet_start_time))
        if control_dist_km < brevet_dist_km:
            while control_dist_km < brevet_list[brevet_adj['list_val']]:
                brevet_adj = brevet_end_list[str(brevet_list[brevet_adj['list_val'] - 1])]
            dist_calculated = control_dist_km / float(brevet_adj['min_open'])
            dist_whole = int(dist_calculated)
            dist_partial = dist_calculated - dist_whole
            brevet_start_time = brevet_start_time.shift(hours=+dist_whole, minutes=+int(dist_partial*60))
        else:
            brevet_start_time = brevet_start_time.shift(hour =+ brevet_start_time['max_time'])
    except TypeError:
        return "Type Error"
    except:
        return "Exception Caught"

    finally:
        return brevet_start_time
def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if str(brevet_dist_km) not in brevet_end_list:
        return "Proper Time"
    if control_dist_km <= 0:
        return "Positive Integer"
    try:
        dist_calculated = 0.0
        dist_whole = 0
        dist_partial = 0.0
        brevet_adj = brevet_end_list[str(brevet_dist_km)]
        brevet_start_time = arrow.get(str.format(brevet_start_time))
        if control_dist_km < brevet_dist_km:
            while control_dist_km < brevet_list[brevet_adj['list_val']]:
                brevet_adj = brevet_end_list[str(brevet_list[brevet_adj['list_val'] - 1])]
            dist_calculated = control_dist_km / float(brevet_adj['max_close'])
            dist_whole = int(dist_calculated)
            dist_partial = dist_calculated - dist_whole
            brevet_start_time = brevet_start_time.shift(hours=+dist_whole, minutes=+int(dist_partial*60))
        else:
            brevet_start_time = brevet_start_time.shift(hour =+ brevet_start_time['max_time'])
    except TypeError:
        return "Type Error"
    except:
        return "Exception Caught"

    finally:
        return brevet_start_time
