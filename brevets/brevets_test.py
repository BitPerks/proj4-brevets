import acp_times
import logging
#import flask_brevets
#import arrow


def test_brevet_calculation():
    import arrow
    test_value = arrow.now().isoformat()
    final_value = arrow.get(str.format(test_value))
    logging.debug(acp_times.open_time(50, 200, test_value))
    logging.debug(final_value.shift(hours =+ 1, minutes=+28))
    assert acp_times.open_time(50, 200, test_value) == final_value.shift(hours =+ 1, minutes=+225)


def test_flask_implement():
    import flask
    test = flask_brevets.app.test_client()
    result = test.get("/_calc_times?km=200&brevet_distance=400&timestamp='2013-05-06T21:24:49.552236-07:00'")
    logging.debug(result)
    assert result