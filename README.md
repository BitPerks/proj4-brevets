# Project 4: Brevet time calculator with Ajax

A calculator used to determine Brevet open and close times

Based on rules and regulations as per; https://rusa.org/pages/acp-brevet-control-times-calculator and https://rusa.org/pages/rulesForRiders
to determine conditions and outputs.

Input Miles/Km into the fields, select a total distance and set a date and time. Calculator will than determine open
and close times based on your information.

The calculations are set up in such a way that rates are determined in a x <= major_points state.
Total race length time is determined from the rules listed above. acp_times will keep track of
total distance and determine the appropriate times for maximum closing time

Current calculations are unfinished due to being unable to find a way to send date/time information properly
to the flask server.

## Author
Forked from: UOCIS322
Isaac Perks: iperks@uoregon.edu 
